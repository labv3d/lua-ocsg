
Inspired by OpenScad, this library provides rendering capabilities for CSG models, based on the OpenCSG library.
Boolean operations (union, intersection, difference) are computed while rendering, providing an efficient way to preview models.

For reference:
	- [OpenSCad](https://openscad.org)
	- [OpenSCG](http://opencsg.osg)

[[_TOC_]]

## class **ocsg**

The library is a simple lua module, used as a class.

The module keep an active list of primitives, so one does not have to send the geometry
at every render.

```lua
ocsg=require("ocsg")
```

#### **init ( )**

  Initialize a new drawing window. Returns the window id.

  For now, a single window of size 640x480 is provided, without any configuration option.

#### **uninit ( )**

  Close the window opened by **init()**, if there is one.

#### **clear ( )**

  Erase any drawing primitives currently active.
  -> call this when sending new geometry

#### **draw (op,poly)**

  Add a new drawing primitive.
  The operation **op** can be '+', '*' or '-' to signify union, intersection or difference.

  The geometry is sent as a list **poly** containing points={x1,y1,z1,x2,y2,2,...} for 3D points and faces={a1,a2,a3,b1,b2,b3,...} for face point indexes.
  -> all faces are assumed to be triangles.
  -> all indexes start at 1, not 0

  It is important to note that when adding a primitive, its operation will be performed
*against* whatever geometry has already been sent.

#### **render ( )**

  Perform a render of the active primitives.
  Also check user input and ajust the camera position.
  -> this must be called in a loop, ideally inside a coroutine.

