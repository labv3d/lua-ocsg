# lua-ocsg

lua library for CSG rendering with [OpenCSG](http://opencsg.org/) library.

This library can render models without computing boolean operation explicitely, but rather
during the rendering in the graphics card.

This allows fast preview of CSG model trees.

This version is a minimal binding, which does not handle color, transparency, or sophisticated user interface.

## Basic usage

```lua
ocsg=require("ocsg")
-- list of 3d points, flattened
p={0., 0., 0.612372,
   -0.288675, -0.5, -0.204124,
   -0.288675, 0.5, -0.204124,
   0.57735, 0., -0.204124}
-- list of face indexes, (starting at 1)
f={2, 3, 4, 3, 2, 1, 4, 1, 2, 1, 4, 3}

ocsg.init()
ocsg.clear()
ocsg.draw("+",{points=p,faces=f})

-- loop this, in a coroutine
ocsg.render()
```

# Building and Dependencies


### On Linux (Mint/Ubuntu/Debian)

sudo apt-get install libglfw3-dev libglew-dev libopencsg-dev


### On Linux (Arch)

sudo pacman -S opencsg glfw-x11

### Windows and Mac

(did not check)


# TODO

* support color, highlighting, transparency
* better user inferface

