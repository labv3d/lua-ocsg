
o=require("ocsg")

p={0., 0., 0.612372,
  -0.288675, -0.5, -0.204124,
  -0.288675, 0.5, -0.204124,
  0.57735, 0., -0.204124}

f={2, 3, 4, 3, 2, 1, 4, 1, 2, 1, 4, 3}


-- move the points lower
q={}
for i=1,#p,3 do
	q[i]=p[i]		-- x
	q[i+1]=p[i+1]	-- y
	q[i+2]=p[i+2]+0.5   -- z
end

-- use a coroutine to loop the interactive display

renderer = coroutine.create(
    function()
		o.init()
		o.clear()
		o.draw("+",{points=q,faces=f})
		o.draw("-",{points=p,faces=f})
        ::loop::
		o.render()
        k=coroutine.yield()
        goto loop
    end
)



local socket = require('socket')

for i = 1, 1000000 do
    coroutine.resume(renderer,i)
    socket.sleep(0.01)
end



