--
-- using nef module simplifies geometry creation...
--

nef=require("nef")

o=require("ocsg")

a=nef.cube{}
b=nef.sphere{n=32}

-- use a coroutine to loop the interactive display

renderer = coroutine.create(
    function()
		o.init()
		o.clear()
		o.draw("+",a:poly{})
		o.draw("-",b:poly{})
        ::loop::
		o.render()
        k=coroutine.yield()
        goto loop
    end
)



local socket = require('socket')

for i = 1, 1000000 do
    coroutine.resume(renderer,i)
    socket.sleep(0.01)
end



